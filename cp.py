# -*- coding: utf-8 -*-

def cp(plt, data, ticks, labels):
    def draw_line(plt, x_point, y1, y2):
        plt.plot([x_point, x_point], [y1, y2], 'k-', lw = 2)

    for row in data:
        plt.plot(ticks, row, linewidth=2)
        # plt.plot(ticks, row, linewidth=2, color = 'green', label = 'grafico1')

    # get axis
    ax = plt.gca()

    ax.axes.get_xaxis().set_ticklabels(labels)
    ax.axes.get_yaxis().set_ticklabels([])
    y1 = ax.get_ylim()[0]
    y2 = ax.get_ylim()[1]

    for value in ticks:
        draw_line(plt, value, y1, y2)
    plt.legend()
    plt.show()

import matplotlib.pyplot as plt
import numpy as np
labels=['attr1', 'attr2', 'attr3', 'attr4', 'attr5', 'attr6']
ticks=[0,2,4,6,8,10]
# data = [
#         [0.7,-1.3,-0.4,0.3,0.1,0.7],
#         [1.6,0.2,0.6,-0.1,0.4,1.3]
# ]
data =  np.random.rand(3, 6)
cp(plt, data, ticks, labels)
