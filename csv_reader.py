import csv
import datetime

reader = list(csv.reader(open('moviedaily.csv', 'r'), delimiter='\t'))

def accumulated_views(reader, title):
    accumulated_views = {}
    accumulated_views[0] = 0
    count = 0
    for line in reader:
        if count == 0:
            count = count + 1
            continue
        if line[1] == title and count <= 70:
            if(count == 1):
                accumulated_views[count] = int(line[3])
            else:
                day = int(line[2])
                accumulated_views[day] = accumulated_views[day - 1] + int(line[3])
            count = count + 1

    return accumulated_views

ABM_acc_views = accumulated_views(reader, 'A Beautiful Mind')
BAT_acc_views = accumulated_views(reader, 'Batman')
GLAD_acc_views = accumulated_views(reader, 'Gladiator')
TNC_acc_views = accumulated_views(reader, 'Titanic')

# TESTING
# for k,v in ABM_acc_views.items():
#     print(k,v)

# # PREPROCESSING
# # DAILY_PER_THEATER FOR 'A BEAUTIFUL MIND'
count = 0

ABM_DAILY_PER_THEATER = {}
BAT_DAILY_PER_THEATER = {}
GLAD_DAILY_PER_THEATER = {}
TNC_DAILY_PER_THEATER = {}

for line in reader:
    if count == 0:
        count = count + 1
        continue
    if line[1] == 'A Beautiful Mind' and int(line[2]) <= 70:
        day = int(line[2])
        ABM_DAILY_PER_THEATER[day] = line[3]
    elif line[1] == 'Batman' and int(line[2]) <= 70:
        day = int(line[2])
        BAT_DAILY_PER_THEATER[day] = line[3]
    elif line[1] == 'Gladiator' and int(line[2]) <= 70:
        day = int(line[2])
        GLAD_DAILY_PER_THEATER[day] = line[3]
    elif line[1] == 'Titanic' and int(line[2]) <= 70:
        day = int(line[2])
        TNC_DAILY_PER_THEATER[day] = line[3]
    count = count + 1


# # # MATPLOTLIB
import matplotlib.pyplot as plt
plt.plot(ABM_DAILY_PER_THEATER.keys(), ABM_DAILY_PER_THEATER.values(), label='Visitas diarias de "A Beautiful Mind"')
plt.plot(BAT_DAILY_PER_THEATER.keys(), BAT_DAILY_PER_THEATER.values(), label='Visitas diarias de "Batman')
plt.plot(GLAD_DAILY_PER_THEATER.keys(), GLAD_DAILY_PER_THEATER.values(), label='Visitas diarias de "Gladiator"')
plt.plot(TNC_DAILY_PER_THEATER.keys(), TNC_DAILY_PER_THEATER.values(), label='Visitas diarias de "Titanic"')
plt.legend(loc=2)
plt.show()

plt.plot(ABM_acc_views.keys(), ABM_acc_views.values(), label='Visitas acumuladas de "A Beautiful Mind"')
plt.plot(BAT_acc_views.keys(), BAT_acc_views.values(), label='Visitas acumuladas de "Batman"')
plt.plot(GLAD_acc_views.keys(), GLAD_acc_views.values(), label='Visitas acumuladas de "Gladiator"')
plt.plot(TNC_acc_views.keys(), TNC_acc_views.values(), label='Visitas acumuladas de "Titanic"')

plt.legend(loc=2)
plt.show()
